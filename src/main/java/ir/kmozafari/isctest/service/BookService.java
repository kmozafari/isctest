package ir.kmozafari.isctest.service;

import ir.kmozafari.isctest.data.entity.Book;
import ir.kmozafari.isctest.data.repository.BookRepository;
import ir.kmozafari.isctest.data.repository.MyEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by kourosh on 12/23/17.
 */

@Service
public class BookService {

    @Autowired
    private BookRepository repository;

    @Autowired
    private MyEntityRepository myEntityRepository;

    public void insert(Book b) {
        repository.save(b);
    }

    public List<Book> getAll() {
        return repository.findAll();
    }

    public List<Book> getAll2(){
        return myEntityRepository.getAllBook();
    }
}
