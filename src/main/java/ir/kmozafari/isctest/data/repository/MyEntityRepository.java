package ir.kmozafari.isctest.data.repository;

import ir.kmozafari.isctest.data.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kourosh on 12/23/17.
 */

@Repository
public interface MyEntityRepository extends JpaRepository<Book, Long>, MyEntityRepositoryCustom {
}
