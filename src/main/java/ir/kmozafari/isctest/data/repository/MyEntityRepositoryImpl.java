package ir.kmozafari.isctest.data.repository;

import ir.kmozafari.isctest.data.entity.Book;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by kourosh on 12/23/17.
 */

@Repository
public class MyEntityRepositoryImpl implements MyEntityRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Book> getAllBook() {
        Query query = null;
        try {
            query = entityManager.createNativeQuery("select * from book", Class.forName("ir.kmozafari.isctest.data.entity.Book"));
            return query.getResultList();
            //return list;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
