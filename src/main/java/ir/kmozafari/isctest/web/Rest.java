package ir.kmozafari.isctest.web;

import ir.kmozafari.isctest.data.entity.Book;
import ir.kmozafari.isctest.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by kourosh on 12/23/17.
 */

@RestController
public class Rest {

    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.GET, path = "/hi")
    public String test() {
        return "Hello";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/add")
    public void AddBook(@RequestBody Book book) {
        bookService.insert(book);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getbooks")
    public String getAllBooks() {
        List<Book> all2 = bookService.getAll2();
        String str = "";
        for (Book book : all2) {
            str += (book.getName() + " " + book.getIsbn() + " " + book.getPages() + "\n");
        }
        return str;
    }
}
