package ir.kmozafari.isctest.data.repository;

import ir.kmozafari.isctest.data.entity.Book;

import java.util.List;

/**
 * Created by kourosh on 12/23/17.
 */
public interface MyEntityRepositoryCustom {
    List<Book> getAllBook();
}
